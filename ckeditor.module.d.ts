/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
export declare class CKEditorModule {
}
export * from './ckeditor';
export { CKEditorComponent } from './ckeditor.component';
