/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
/**
 * Basic typings for the CKEditor4 elements.
 */
export var CKEditor4;
(function (CKEditor4) {
    /**
     * The CKEditor4 editor constructor.
     * @record
     */
    function Config() { }
    CKEditor4.Config = Config;
    /**
     * The event object passed to CKEditor4 event callbacks.
     *
     * See https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_eventInfo.html
     * to learn more.
     * @record
     */
    function EventInfo() { }
    CKEditor4.EventInfo = EventInfo;
    if (false) {
        /** @type {?} */
        EventInfo.prototype.name;
        /** @type {?} */
        EventInfo.prototype.editor;
        /** @type {?} */
        EventInfo.prototype.data;
        /** @type {?} */
        EventInfo.prototype.listenerData;
        /** @type {?} */
        EventInfo.prototype.sender;
        /**
         * @return {?}
         */
        EventInfo.prototype.cancel = function () { };
        /**
         * @return {?}
         */
        EventInfo.prototype.removeListener = function () { };
        /**
         * @return {?}
         */
        EventInfo.prototype.stop = function () { };
    }
})(CKEditor4 || (CKEditor4 = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2tlZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ja2VkaXRvcjQtYW5ndWxhci8iLCJzb3VyY2VzIjpbImNrZWRpdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBUUEsTUFBTSxLQUFXLFNBQVMsQ0E0Q3pCO0FBNUNELFdBQWlCLFNBQVM7Ozs7O0lBS3pCLHFCQUVDOzs7Ozs7Ozs7SUFrQkQsd0JBa0JDOzs7O1FBakJBLHlCQUFzQjs7UUFDdEIsMkJBQXFCOztRQUNyQix5QkFFRTs7UUFDRixpQ0FFRTs7UUFDRiwyQkFFRTs7OztRQUVGLDZDQUFlOzs7O1FBRWYscURBQXVCOzs7O1FBRXZCLDJDQUFhOztBQUVmLENBQUMsRUE1Q2dCLFNBQVMsS0FBVCxTQUFTLFFBNEN6QiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGxpY2Vuc2UgQ29weXJpZ2h0IChjKSAyMDAzLTIwMTksIENLU291cmNlIC0gRnJlZGVyaWNvIEtuYWJiZW4uIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKiBGb3IgbGljZW5zaW5nLCBzZWUgTElDRU5TRS5tZC5cbiAqL1xuXG4vKipcbiAqIEJhc2ljIHR5cGluZ3MgZm9yIHRoZSBDS0VkaXRvcjQgZWxlbWVudHMuXG4gKi9cbmV4cG9ydCBuYW1lc3BhY2UgQ0tFZGl0b3I0IHtcblxuXHQvKipcblx0ICogVGhlIENLRWRpdG9yNCBlZGl0b3IgY29uc3RydWN0b3IuXG5cdCAqL1xuXHRleHBvcnQgaW50ZXJmYWNlIENvbmZpZyB7XG5cdFx0WyBrZXk6IHN0cmluZyBdOiBhbnk7XG5cdH1cblxuXHQvKipcblx0ICogVGhlIENLRWRpdG9yNCBlZGl0b3IgaW50ZXJmYWNlIHR5cGUuXG5cdCAqIFNlZSBodHRwczovL2NrZWRpdG9yLmNvbS9kb2NzL2NrZWRpdG9yNC9sYXRlc3QvZ3VpZGUvZGV2X3VpdHlwZXMuaHRtbFxuXHQgKiB0byBsZWFybiBtb3JlLlxuXHQgKi9cblx0ZXhwb3J0IGNvbnN0IGVudW0gRWRpdG9yVHlwZSB7XG5cdFx0RElWQVJFQSA9ICdkaXZhcmVhJyxcblx0XHRJTkxJTkUgPSAnaW5saW5lJ1xuXHR9XG5cblx0LyoqXG5cdCAqIFRoZSBldmVudCBvYmplY3QgcGFzc2VkIHRvIENLRWRpdG9yNCBldmVudCBjYWxsYmFja3MuXG5cdCAqXG5cdCAqIFNlZSBodHRwczovL2NrZWRpdG9yLmNvbS9kb2NzL2NrZWRpdG9yNC9sYXRlc3QvYXBpL0NLRURJVE9SX2V2ZW50SW5mby5odG1sXG5cdCAqIHRvIGxlYXJuIG1vcmUuXG5cdCAqL1xuXHRleHBvcnQgaW50ZXJmYWNlIEV2ZW50SW5mbyB7XG5cdFx0cmVhZG9ubHkgbmFtZTogc3RyaW5nO1xuXHRcdHJlYWRvbmx5IGVkaXRvcjogYW55O1xuXHRcdHJlYWRvbmx5IGRhdGE6IHtcblx0XHRcdFsga2V5OiBzdHJpbmcgXTogYW55O1xuXHRcdH07XG5cdFx0cmVhZG9ubHkgbGlzdGVuZXJEYXRhOiB7XG5cdFx0XHRbIGtleTogc3RyaW5nIF06IGFueTtcblx0XHR9O1xuXHRcdHJlYWRvbmx5IHNlbmRlcjoge1xuXHRcdFx0WyBrZXk6IHN0cmluZyBdOiBhbnk7XG5cdFx0fTtcblxuXHRcdGNhbmNlbCgpOiB2b2lkO1xuXG5cdFx0cmVtb3ZlTGlzdGVuZXIoKTogdm9pZDtcblxuXHRcdHN0b3AoKTogdm9pZDtcblx0fVxufVxuIl19