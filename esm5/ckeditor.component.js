/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
import { Component, NgZone, Input, Output, EventEmitter, forwardRef, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { getEditorNamespace } from './ckeditor.helpers';
var CKEditorComponent = /** @class */ (function () {
    function CKEditorComponent(elementRef, ngZone) {
        this.elementRef = elementRef;
        this.ngZone = ngZone;
        /**
         * Tag name of the editor component.
         *
         * The default tag is `textarea`.
         */
        this.tagName = 'textarea';
        /**
         * The type of the editor interface.
         *
         * By default editor interface will be initialized as `divarea` editor which is an inline editor with fixed UI.
         * You can change interface type by choosing between `divarea` and `inline` editor interface types.
         *
         * See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_uitypes.html
         * and https://ckeditor.com/docs/ckeditor4/latest/examples/fixedui.html
         * to learn more.
         */
        this.type = "divarea" /* DIVAREA */;
        /**
         * Fires when the editor is ready. It corresponds with the `editor#instanceReady`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-instanceReady
         * event.
         */
        this.ready = new EventEmitter();
        /**
         * Fires when the content of the editor has changed. It corresponds with the `editor#change`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-change
         * event. For performance reasons this event may be called even when data didn't really changed.
         */
        this.change = new EventEmitter();
        /**
         * Fires when the content of the editor has changed. In contrast to `change` - only emits when
         * data really changed thus can be successfully used with `[data]` and two way `[(data)]` binding.
         *
         * See more: https://angular.io/guide/template-syntax#two-way-binding---
         */
        this.dataChange = new EventEmitter();
        /**
         * Fires when the editing view of the editor is focused. It corresponds with the `editor#focus`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-focus
         * event.
         */
        this.focus = new EventEmitter();
        /**
         * Fires when the editing view of the editor is blurred. It corresponds with the `editor#blur`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-blur
         * event.
         */
        this.blur = new EventEmitter();
        /**
         * If the component is read–only before the editor instance is created, it remembers that state,
         * so the editor can become read–only once it is ready.
         */
        this._readOnly = null;
        this._data = null;
        /**
         * CKEditor 4 script url address. Script will be loaded only if CKEDITOR namespace is missing.
         *
         * Defaults to 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js'
         */
        this.editorUrl = 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js';
    }
    Object.defineProperty(CKEditorComponent.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return this._data;
        },
        /**
         * Keeps track of the editor's data.
         *
         * It's also decorated as an input which is useful when not using the ngModel.
         *
         * See https://angular.io/api/forms/NgModel to learn more.
         */
        set: /**
         * Keeps track of the editor's data.
         *
         * It's also decorated as an input which is useful when not using the ngModel.
         *
         * See https://angular.io/api/forms/NgModel to learn more.
         * @param {?} data
         * @return {?}
         */
        function (data) {
            if (data === this._data) {
                return;
            }
            if (this.instance) {
                this.instance.setData(data);
                // Data may be changed by ACF.
                this._data = this.instance.getData();
                return;
            }
            this._data = data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CKEditorComponent.prototype, "readOnly", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.instance) {
                return this.instance.readOnly;
            }
            return this._readOnly;
        },
        /**
         * When set `true`, the editor becomes read-only.
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#property-readOnly
         * to learn more.
         */
        set: /**
         * When set `true`, the editor becomes read-only.
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#property-readOnly
         * to learn more.
         * @param {?} isReadOnly
         * @return {?}
         */
        function (isReadOnly) {
            if (this.instance) {
                this.instance.setReadOnly(isReadOnly);
                return;
            }
            // Delay setting read-only state until editor initialization.
            this._readOnly = isReadOnly;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        getEditorNamespace(this.editorUrl).then((/**
         * @return {?}
         */
        function () {
            _this.ngZone.runOutsideAngular(_this.createEditor.bind(_this));
        })).catch(window.console.error);
    };
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        function () {
            if (_this.instance) {
                _this.instance.destroy();
                _this.instance = null;
            }
        }));
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CKEditorComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.data = value;
    };
    /**
     * @param {?} callback
     * @return {?}
     */
    CKEditorComponent.prototype.registerOnChange = /**
     * @param {?} callback
     * @return {?}
     */
    function (callback) {
        this.onChange = callback;
    };
    /**
     * @param {?} callback
     * @return {?}
     */
    CKEditorComponent.prototype.registerOnTouched = /**
     * @param {?} callback
     * @return {?}
     */
    function (callback) {
        this.onTouched = callback;
    };
    /**
     * @private
     * @return {?}
     */
    CKEditorComponent.prototype.createEditor = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var element = this.createInitialElement();
        this.config = this.ensureDivareaPlugin(this.config || {});
        /** @type {?} */
        var instance = this.type === "inline" /* INLINE */ ?
            CKEDITOR.inline(element, this.config)
            : CKEDITOR.replace(element, this.config);
        instance.once('instanceReady', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            var _this = this;
            this.instance = instance;
            this.wrapper.removeAttribute('style');
            this.elementRef.nativeElement.appendChild(this.wrapper);
            // Read only state may change during instance initialization.
            this.readOnly = this._readOnly !== null ? this._readOnly : this.instance.readOnly;
            this.subscribe(this.instance);
            /** @type {?} */
            var undo = instance.undoManager;
            if (this.data !== null) {
                undo && undo.lock();
                instance.setData(this.data);
                // Locking undoManager prevents 'change' event.
                // Trigger it manually to updated bound data.
                if (this.data !== instance.getData()) {
                    instance.fire('change');
                }
                undo && undo.unlock();
            }
            this.ngZone.run((/**
             * @return {?}
             */
            function () {
                _this.ready.emit(evt);
            }));
        }), this);
    };
    /**
     * @private
     * @param {?} editor
     * @return {?}
     */
    CKEditorComponent.prototype.subscribe = /**
     * @private
     * @param {?} editor
     * @return {?}
     */
    function (editor) {
        var _this = this;
        editor.on('focus', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                _this.focus.emit(evt);
            }));
        }));
        editor.on('blur', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                if (_this.onTouched) {
                    _this.onTouched();
                }
                _this.blur.emit(evt);
            }));
        }));
        editor.on('change', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var newData = editor.getData();
                _this.change.emit(evt);
                if (newData === _this.data) {
                    return;
                }
                _this._data = newData;
                _this.dataChange.emit(newData);
                if (_this.onChange) {
                    _this.onChange(newData);
                }
            }));
        }));
    };
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    CKEditorComponent.prototype.ensureDivareaPlugin = /**
     * @private
     * @param {?} config
     * @return {?}
     */
    function (config) {
        var extraPlugins = config.extraPlugins, removePlugins = config.removePlugins;
        extraPlugins = this.removePlugin(extraPlugins, 'divarea') || '';
        extraPlugins = extraPlugins.concat(typeof extraPlugins === 'string' ? ',divarea' : 'divarea');
        if (removePlugins && removePlugins.includes('divarea')) {
            removePlugins = this.removePlugin(removePlugins, 'divarea');
            console.warn('[CKEDITOR] divarea plugin is required to initialize editor using Angular integration.');
        }
        return Object.assign({}, config, { extraPlugins: extraPlugins, removePlugins: removePlugins });
    };
    /**
     * @private
     * @param {?} plugins
     * @param {?} toRemove
     * @return {?}
     */
    CKEditorComponent.prototype.removePlugin = /**
     * @private
     * @param {?} plugins
     * @param {?} toRemove
     * @return {?}
     */
    function (plugins, toRemove) {
        if (!plugins) {
            return null;
        }
        /** @type {?} */
        var isString = typeof plugins === 'string';
        if (isString) {
            plugins = ((/** @type {?} */ (plugins))).split(',');
        }
        plugins = ((/** @type {?} */ (plugins))).filter((/**
         * @param {?} plugin
         * @return {?}
         */
        function (plugin) { return plugin !== toRemove; }));
        if (isString) {
            plugins = ((/** @type {?} */ (plugins))).join(',');
        }
        return plugins;
    };
    /**
     * @private
     * @return {?}
     */
    CKEditorComponent.prototype.createInitialElement = /**
     * @private
     * @return {?}
     */
    function () {
        // Render editor outside of component so it won't be removed from DOM before `instanceReady`.
        this.wrapper = document.createElement('div');
        /** @type {?} */
        var element = document.createElement(this.tagName);
        this.wrapper.setAttribute('style', 'display:none;');
        document.body.appendChild(this.wrapper);
        this.wrapper.appendChild(element);
        return element;
    };
    CKEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ckeditor',
                    template: '<ng-template></ng-template>',
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return CKEditorComponent; })),
                            multi: true,
                        }
                    ]
                }] }
    ];
    /** @nocollapse */
    CKEditorComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NgZone }
    ]; };
    CKEditorComponent.propDecorators = {
        config: [{ type: Input }],
        tagName: [{ type: Input }],
        type: [{ type: Input }],
        data: [{ type: Input }],
        readOnly: [{ type: Input }],
        ready: [{ type: Output }],
        change: [{ type: Output }],
        dataChange: [{ type: Output }],
        focus: [{ type: Output }],
        blur: [{ type: Output }],
        editorUrl: [{ type: Input }]
    };
    return CKEditorComponent;
}());
export { CKEditorComponent };
if (false) {
    /**
     * The configuration of the editor.
     * See https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html
     * to learn more.
     * @type {?}
     */
    CKEditorComponent.prototype.config;
    /**
     * Tag name of the editor component.
     *
     * The default tag is `textarea`.
     * @type {?}
     */
    CKEditorComponent.prototype.tagName;
    /**
     * The type of the editor interface.
     *
     * By default editor interface will be initialized as `divarea` editor which is an inline editor with fixed UI.
     * You can change interface type by choosing between `divarea` and `inline` editor interface types.
     *
     * See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_uitypes.html
     * and https://ckeditor.com/docs/ckeditor4/latest/examples/fixedui.html
     * to learn more.
     * @type {?}
     */
    CKEditorComponent.prototype.type;
    /**
     * Fires when the editor is ready. It corresponds with the `editor#instanceReady`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-instanceReady
     * event.
     * @type {?}
     */
    CKEditorComponent.prototype.ready;
    /**
     * Fires when the content of the editor has changed. It corresponds with the `editor#change`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-change
     * event. For performance reasons this event may be called even when data didn't really changed.
     * @type {?}
     */
    CKEditorComponent.prototype.change;
    /**
     * Fires when the content of the editor has changed. In contrast to `change` - only emits when
     * data really changed thus can be successfully used with `[data]` and two way `[(data)]` binding.
     *
     * See more: https://angular.io/guide/template-syntax#two-way-binding---
     * @type {?}
     */
    CKEditorComponent.prototype.dataChange;
    /**
     * Fires when the editing view of the editor is focused. It corresponds with the `editor#focus`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-focus
     * event.
     * @type {?}
     */
    CKEditorComponent.prototype.focus;
    /**
     * Fires when the editing view of the editor is blurred. It corresponds with the `editor#blur`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-blur
     * event.
     * @type {?}
     */
    CKEditorComponent.prototype.blur;
    /**
     * The instance of the editor created by this component.
     * @type {?}
     */
    CKEditorComponent.prototype.instance;
    /**
     * Wrapper element used to initialize editor.
     * @type {?}
     */
    CKEditorComponent.prototype.wrapper;
    /**
     * If the component is read–only before the editor instance is created, it remembers that state,
     * so the editor can become read–only once it is ready.
     * @type {?}
     * @private
     */
    CKEditorComponent.prototype._readOnly;
    /**
     * A callback executed when the content of the editor changes. Part of the
     * `ControlValueAccessor` (https://angular.io/api/forms/ControlValueAccessor) interface.
     *
     * Note: Unset unless the component uses the `ngModel`.
     * @type {?}
     */
    CKEditorComponent.prototype.onChange;
    /**
     * A callback executed when the editor has been blurred. Part of the
     * `ControlValueAccessor` (https://angular.io/api/forms/ControlValueAccessor) interface.
     *
     * Note: Unset unless the component uses the `ngModel`.
     * @type {?}
     */
    CKEditorComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    CKEditorComponent.prototype._data;
    /**
     * CKEditor 4 script url address. Script will be loaded only if CKEDITOR namespace is missing.
     *
     * Defaults to 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js'
     * @type {?}
     */
    CKEditorComponent.prototype.editorUrl;
    /**
     * @type {?}
     * @private
     */
    CKEditorComponent.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    CKEditorComponent.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2tlZGl0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY2tlZGl0b3I0LWFuZ3VsYXIvIiwic291cmNlcyI6WyJja2VkaXRvci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFLQSxPQUFPLEVBQ04sU0FBUyxFQUNULE1BQU0sRUFDTixLQUFLLEVBQ0wsTUFBTSxFQUNOLFlBQVksRUFDWixVQUFVLEVBQ1YsVUFBVSxFQUVWLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFFTixpQkFBaUIsRUFDakIsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU14RDtJQXNLQywyQkFBcUIsVUFBc0IsRUFBVSxNQUFjO1FBQTlDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFROzs7Ozs7UUE3STFELFlBQU8sR0FBRyxVQUFVLENBQUM7Ozs7Ozs7Ozs7O1FBWXJCLFNBQUksMkJBQXNEOzs7Ozs7UUF5RHpELFVBQUssR0FBRyxJQUFJLFlBQVksRUFBdUIsQ0FBQzs7Ozs7O1FBT2hELFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBdUIsQ0FBQzs7Ozs7OztRQVFqRCxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQXVCLENBQUM7Ozs7OztRQU9yRCxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQXVCLENBQUM7Ozs7OztRQU9oRCxTQUFJLEdBQUcsSUFBSSxZQUFZLEVBQXVCLENBQUM7Ozs7O1FBZ0JqRCxjQUFTLEdBQVksSUFBSSxDQUFDO1FBa0IxQixVQUFLLEdBQVcsSUFBSSxDQUFDOzs7Ozs7UUFPcEIsY0FBUyxHQUFHLDBEQUEwRCxDQUFDO0lBR2hGLENBQUM7SUF6SEQsc0JBQWEsbUNBQUk7Ozs7UUFnQmpCO1lBQ0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ25CLENBQUM7UUF6QkQ7Ozs7OztXQU1HOzs7Ozs7Ozs7O1FBQ0gsVUFBbUIsSUFBWTtZQUM5QixJQUFLLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFHO2dCQUMxQixPQUFPO2FBQ1A7WUFFRCxJQUFLLElBQUksQ0FBQyxRQUFRLEVBQUc7Z0JBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFFLElBQUksQ0FBRSxDQUFDO2dCQUM5Qiw4QkFBOEI7Z0JBQzlCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDckMsT0FBTzthQUNQO1lBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFFbkIsQ0FBQzs7O09BQUE7SUFXRCxzQkFBYSx1Q0FBUTs7OztRQVVyQjtZQUNDLElBQUssSUFBSSxDQUFDLFFBQVEsRUFBRztnQkFDcEIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQzthQUM5QjtZQUVELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN2QixDQUFDO1FBckJEOzs7O1dBSUc7Ozs7Ozs7O1FBQ0gsVUFBdUIsVUFBbUI7WUFDekMsSUFBSyxJQUFJLENBQUMsUUFBUSxFQUFHO2dCQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBRSxVQUFVLENBQUUsQ0FBQztnQkFDeEMsT0FBTzthQUNQO1lBRUQsNkRBQTZEO1lBQzdELElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO1FBQzdCLENBQUM7OztPQUFBOzs7O0lBMEZELDJDQUFlOzs7SUFBZjtRQUFBLGlCQUlDO1FBSEEsa0JBQWtCLENBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBRSxDQUFDLElBQUk7OztRQUFFO1lBQzFDLEtBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUUsS0FBSSxDQUFFLENBQUUsQ0FBQztRQUNqRSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUUsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsdUNBQVc7OztJQUFYO1FBQUEsaUJBT0M7UUFOQSxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQjs7O1FBQUU7WUFDOUIsSUFBSyxLQUFJLENBQUMsUUFBUSxFQUFHO2dCQUNwQixLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN4QixLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUNyQjtRQUNGLENBQUMsRUFBRSxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxzQ0FBVTs7OztJQUFWLFVBQVksS0FBYTtRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUNuQixDQUFDOzs7OztJQUVELDRDQUFnQjs7OztJQUFoQixVQUFrQixRQUFrQztRQUNuRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELDZDQUFpQjs7OztJQUFqQixVQUFtQixRQUFvQjtRQUN0QyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVPLHdDQUFZOzs7O0lBQXBCOztZQUNPLE9BQU8sR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7UUFFM0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUUsSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUUsQ0FBQzs7WUFFdEQsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLDBCQUFnQyxDQUFDLENBQUM7WUFDM0QsUUFBUSxDQUFDLE1BQU0sQ0FBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBRTtZQUN2QyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBRTtRQUUzQyxRQUFRLENBQUMsSUFBSSxDQUFFLGVBQWU7Ozs7UUFBRSxVQUFVLEdBQUc7WUFBYixpQkE2Qi9CO1lBNUJBLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1lBRXpCLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFFLE9BQU8sQ0FBRSxDQUFDO1lBRXhDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBRSxJQUFJLENBQUMsT0FBTyxDQUFFLENBQUM7WUFFMUQsNkRBQTZEO1lBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBRWxGLElBQUksQ0FBQyxTQUFTLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBRSxDQUFDOztnQkFFMUIsSUFBSSxHQUFHLFFBQVEsQ0FBQyxXQUFXO1lBRWpDLElBQUssSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEVBQUc7Z0JBQ3pCLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxPQUFPLENBQUUsSUFBSSxDQUFDLElBQUksQ0FBRSxDQUFDO2dCQUU5QiwrQ0FBK0M7Z0JBQy9DLDZDQUE2QztnQkFDN0MsSUFBSyxJQUFJLENBQUMsSUFBSSxLQUFLLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBRztvQkFDdkMsUUFBUSxDQUFDLElBQUksQ0FBRSxRQUFRLENBQUUsQ0FBQztpQkFDMUI7Z0JBQ0QsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUN0QjtZQUVELElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRzs7O1lBQUU7Z0JBQ2hCLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFFLEdBQUcsQ0FBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxDQUFDO1FBQ0wsQ0FBQyxHQUFFLElBQUksQ0FBRSxDQUFDO0lBQ1gsQ0FBQzs7Ozs7O0lBRU8scUNBQVM7Ozs7O0lBQWpCLFVBQW1CLE1BQVc7UUFBOUIsaUJBbUNDO1FBbENBLE1BQU0sQ0FBQyxFQUFFLENBQUUsT0FBTzs7OztRQUFFLFVBQUEsR0FBRztZQUN0QixLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUc7OztZQUFFO2dCQUNoQixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBRSxHQUFHLENBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsQ0FBQztRQUNMLENBQUMsRUFBRSxDQUFDO1FBRUosTUFBTSxDQUFDLEVBQUUsQ0FBRSxNQUFNOzs7O1FBQUUsVUFBQSxHQUFHO1lBQ3JCLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRzs7O1lBQUU7Z0JBQ2hCLElBQUssS0FBSSxDQUFDLFNBQVMsRUFBRztvQkFDckIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2lCQUNqQjtnQkFFRCxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBRSxHQUFHLENBQUUsQ0FBQztZQUN2QixDQUFDLEVBQUUsQ0FBQztRQUNMLENBQUMsRUFBRSxDQUFDO1FBRUosTUFBTSxDQUFDLEVBQUUsQ0FBRSxRQUFROzs7O1FBQUUsVUFBQSxHQUFHO1lBQ3ZCLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRzs7O1lBQUU7O29CQUNWLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxFQUFFO2dCQUVoQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBRSxHQUFHLENBQUUsQ0FBQztnQkFFeEIsSUFBSyxPQUFPLEtBQUssS0FBSSxDQUFDLElBQUksRUFBRztvQkFDNUIsT0FBTztpQkFDUDtnQkFFRCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztnQkFDckIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUUsT0FBTyxDQUFFLENBQUM7Z0JBRWhDLElBQUssS0FBSSxDQUFDLFFBQVEsRUFBRztvQkFDcEIsS0FBSSxDQUFDLFFBQVEsQ0FBRSxPQUFPLENBQUUsQ0FBQztpQkFDekI7WUFDRixDQUFDLEVBQUUsQ0FBQztRQUNMLENBQUMsRUFBRSxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sK0NBQW1COzs7OztJQUEzQixVQUE2QixNQUF3QjtRQUM5QyxJQUFBLGtDQUFZLEVBQUUsb0NBQWE7UUFFakMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUUsWUFBWSxFQUFFLFNBQVMsQ0FBRSxJQUFJLEVBQUUsQ0FBQztRQUNsRSxZQUFZLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBRSxPQUFPLFlBQVksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFFLENBQUM7UUFFaEcsSUFBSyxhQUFhLElBQUksYUFBYSxDQUFDLFFBQVEsQ0FBRSxTQUFTLENBQUUsRUFBRztZQUUzRCxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBRSxhQUFhLEVBQUUsU0FBUyxDQUFFLENBQUM7WUFFOUQsT0FBTyxDQUFDLElBQUksQ0FBRSx1RkFBdUYsQ0FBRSxDQUFDO1NBQ3hHO1FBRUQsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxZQUFZLGNBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFFLENBQUM7SUFDckUsQ0FBQzs7Ozs7OztJQUVPLHdDQUFZOzs7Ozs7SUFBcEIsVUFBc0IsT0FBMEIsRUFBRSxRQUFnQjtRQUNqRSxJQUFLLENBQUMsT0FBTyxFQUFHO1lBQ2YsT0FBTyxJQUFJLENBQUM7U0FDWjs7WUFFSyxRQUFRLEdBQUcsT0FBTyxPQUFPLEtBQUssUUFBUTtRQUU1QyxJQUFLLFFBQVEsRUFBRztZQUNmLE9BQU8sR0FBRyxDQUFFLG1CQUFBLE9BQU8sRUFBVSxDQUFFLENBQUMsS0FBSyxDQUFFLEdBQUcsQ0FBRSxDQUFDO1NBQzdDO1FBRUQsT0FBTyxHQUFHLENBQUUsbUJBQUEsT0FBTyxFQUFZLENBQUUsQ0FBQyxNQUFNOzs7O1FBQUUsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLEtBQUssUUFBUSxFQUFuQixDQUFtQixFQUFFLENBQUM7UUFFMUUsSUFBSyxRQUFRLEVBQUc7WUFDZixPQUFPLEdBQUcsQ0FBRSxtQkFBQSxPQUFPLEVBQVksQ0FBRSxDQUFDLElBQUksQ0FBRSxHQUFHLENBQUUsQ0FBQztTQUM5QztRQUVELE9BQU8sT0FBTyxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRU8sZ0RBQW9COzs7O0lBQTVCO1FBQ0MsNkZBQTZGO1FBQzdGLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBRSxLQUFLLENBQUUsQ0FBQzs7WUFDekMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBRTtRQUV0RCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBRSxPQUFPLEVBQUUsZUFBZSxDQUFFLENBQUM7UUFFdEQsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBRSxDQUFDO1FBQzFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFFLE9BQU8sQ0FBRSxDQUFDO1FBRXBDLE9BQU8sT0FBTyxDQUFDO0lBQ2hCLENBQUM7O2dCQWpVRCxTQUFTLFNBQUU7b0JBQ1gsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLFFBQVEsRUFBRSw2QkFBNkI7b0JBRXZDLFNBQVMsRUFBRTt3QkFDVjs0QkFDQyxPQUFPLEVBQUUsaUJBQWlCOzRCQUMxQixXQUFXLEVBQUUsVUFBVTs7OzRCQUFFLGNBQU0sT0FBQSxpQkFBaUIsRUFBakIsQ0FBaUIsRUFBRTs0QkFDbEQsS0FBSyxFQUFFLElBQUk7eUJBQ1g7cUJBQ0Q7aUJBQ0Q7Ozs7Z0JBMUJBLFVBQVU7Z0JBTFYsTUFBTTs7O3lCQXNDTCxLQUFLOzBCQU9MLEtBQUs7dUJBWUwsS0FBSzt1QkFTTCxLQUFLOzJCQXlCTCxLQUFLO3dCQXVCTCxNQUFNO3lCQU9OLE1BQU07NkJBUU4sTUFBTTt3QkFPTixNQUFNO3VCQU9OLE1BQU07NEJBeUNOLEtBQUs7O0lBOEpQLHdCQUFDO0NBQUEsQUFsVUQsSUFrVUM7U0F0VFksaUJBQWlCOzs7Ozs7OztJQU03QixtQ0FBbUM7Ozs7Ozs7SUFPbkMsb0NBQThCOzs7Ozs7Ozs7Ozs7SUFZOUIsaUNBQW1FOzs7Ozs7O0lBeURuRSxrQ0FBMEQ7Ozs7Ozs7SUFPMUQsbUNBQTJEOzs7Ozs7OztJQVEzRCx1Q0FBK0Q7Ozs7Ozs7SUFPL0Qsa0NBQTBEOzs7Ozs7O0lBTzFELGlDQUF5RDs7Ozs7SUFLekQscUNBQWM7Ozs7O0lBS2Qsb0NBQXFCOzs7Ozs7O0lBTXJCLHNDQUFrQzs7Ozs7Ozs7SUFRbEMscUNBQW9DOzs7Ozs7OztJQVFwQyxzQ0FBdUI7Ozs7O0lBRXZCLGtDQUE2Qjs7Ozs7OztJQU83QixzQ0FBZ0Y7Ozs7O0lBRW5FLHVDQUE4Qjs7Ozs7SUFBRSxtQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEBsaWNlbnNlIENvcHlyaWdodCAoYykgMjAwMy0yMDE5LCBDS1NvdXJjZSAtIEZyZWRlcmljbyBLbmFiYmVuLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICogRm9yIGxpY2Vuc2luZywgc2VlIExJQ0VOU0UubWQuXG4gKi9cblxuaW1wb3J0IHtcblx0Q29tcG9uZW50LFxuXHROZ1pvbmUsXG5cdElucHV0LFxuXHRPdXRwdXQsXG5cdEV2ZW50RW1pdHRlcixcblx0Zm9yd2FyZFJlZixcblx0RWxlbWVudFJlZixcblx0QWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95XG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQge1xuXHRDb250cm9sVmFsdWVBY2Nlc3Nvcixcblx0TkdfVkFMVUVfQUNDRVNTT1Jcbn0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBnZXRFZGl0b3JOYW1lc3BhY2UgfSBmcm9tICcuL2NrZWRpdG9yLmhlbHBlcnMnO1xuXG5pbXBvcnQgeyBDS0VkaXRvcjQgfSBmcm9tICcuL2NrZWRpdG9yJztcblxuZGVjbGFyZSBsZXQgQ0tFRElUT1I6IGFueTtcblxuQENvbXBvbmVudCgge1xuXHRzZWxlY3RvcjogJ2NrZWRpdG9yJyxcblx0dGVtcGxhdGU6ICc8bmctdGVtcGxhdGU+PC9uZy10ZW1wbGF0ZT4nLFxuXG5cdHByb3ZpZGVyczogW1xuXHRcdHtcblx0XHRcdHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuXHRcdFx0dXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoICgpID0+IENLRWRpdG9yQ29tcG9uZW50ICksXG5cdFx0XHRtdWx0aTogdHJ1ZSxcblx0XHR9XG5cdF1cbn0gKVxuZXhwb3J0IGNsYXNzIENLRWRpdG9yQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95LCBDb250cm9sVmFsdWVBY2Nlc3NvciB7XG5cdC8qKlxuXHQgKiBUaGUgY29uZmlndXJhdGlvbiBvZiB0aGUgZWRpdG9yLlxuXHQgKiBTZWUgaHR0cHM6Ly9ja2VkaXRvci5jb20vZG9jcy9ja2VkaXRvcjQvbGF0ZXN0L2FwaS9DS0VESVRPUl9jb25maWcuaHRtbFxuXHQgKiB0byBsZWFybiBtb3JlLlxuXHQgKi9cblx0QElucHV0KCkgY29uZmlnPzogQ0tFZGl0b3I0LkNvbmZpZztcblxuXHQvKipcblx0ICogVGFnIG5hbWUgb2YgdGhlIGVkaXRvciBjb21wb25lbnQuXG5cdCAqXG5cdCAqIFRoZSBkZWZhdWx0IHRhZyBpcyBgdGV4dGFyZWFgLlxuXHQgKi9cblx0QElucHV0KCkgdGFnTmFtZSA9ICd0ZXh0YXJlYSc7XG5cblx0LyoqXG5cdCAqIFRoZSB0eXBlIG9mIHRoZSBlZGl0b3IgaW50ZXJmYWNlLlxuXHQgKlxuXHQgKiBCeSBkZWZhdWx0IGVkaXRvciBpbnRlcmZhY2Ugd2lsbCBiZSBpbml0aWFsaXplZCBhcyBgZGl2YXJlYWAgZWRpdG9yIHdoaWNoIGlzIGFuIGlubGluZSBlZGl0b3Igd2l0aCBmaXhlZCBVSS5cblx0ICogWW91IGNhbiBjaGFuZ2UgaW50ZXJmYWNlIHR5cGUgYnkgY2hvb3NpbmcgYmV0d2VlbiBgZGl2YXJlYWAgYW5kIGBpbmxpbmVgIGVkaXRvciBpbnRlcmZhY2UgdHlwZXMuXG5cdCAqXG5cdCAqIFNlZSBodHRwczovL2NrZWRpdG9yLmNvbS9kb2NzL2NrZWRpdG9yNC9sYXRlc3QvZ3VpZGUvZGV2X3VpdHlwZXMuaHRtbFxuXHQgKiBhbmQgaHR0cHM6Ly9ja2VkaXRvci5jb20vZG9jcy9ja2VkaXRvcjQvbGF0ZXN0L2V4YW1wbGVzL2ZpeGVkdWkuaHRtbFxuXHQgKiB0byBsZWFybiBtb3JlLlxuXHQgKi9cblx0QElucHV0KCkgdHlwZTogQ0tFZGl0b3I0LkVkaXRvclR5cGUgPSBDS0VkaXRvcjQuRWRpdG9yVHlwZS5ESVZBUkVBO1xuXG5cdC8qKlxuXHQgKiBLZWVwcyB0cmFjayBvZiB0aGUgZWRpdG9yJ3MgZGF0YS5cblx0ICpcblx0ICogSXQncyBhbHNvIGRlY29yYXRlZCBhcyBhbiBpbnB1dCB3aGljaCBpcyB1c2VmdWwgd2hlbiBub3QgdXNpbmcgdGhlIG5nTW9kZWwuXG5cdCAqXG5cdCAqIFNlZSBodHRwczovL2FuZ3VsYXIuaW8vYXBpL2Zvcm1zL05nTW9kZWwgdG8gbGVhcm4gbW9yZS5cblx0ICovXG5cdEBJbnB1dCgpIHNldCBkYXRhKCBkYXRhOiBzdHJpbmcgKSB7XG5cdFx0aWYgKCBkYXRhID09PSB0aGlzLl9kYXRhICkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdGlmICggdGhpcy5pbnN0YW5jZSApIHtcblx0XHRcdHRoaXMuaW5zdGFuY2Uuc2V0RGF0YSggZGF0YSApO1xuXHRcdFx0Ly8gRGF0YSBtYXkgYmUgY2hhbmdlZCBieSBBQ0YuXG5cdFx0XHR0aGlzLl9kYXRhID0gdGhpcy5pbnN0YW5jZS5nZXREYXRhKCk7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXG5cdFx0dGhpcy5fZGF0YSA9IGRhdGE7XG5cblx0fVxuXG5cdGdldCBkYXRhKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2RhdGE7XG5cdH1cblxuXHQvKipcblx0ICogV2hlbiBzZXQgYHRydWVgLCB0aGUgZWRpdG9yIGJlY29tZXMgcmVhZC1vbmx5LlxuXHQgKiBodHRwczovL2NrZWRpdG9yLmNvbS9kb2NzL2NrZWRpdG9yNC9sYXRlc3QvYXBpL0NLRURJVE9SX2VkaXRvci5odG1sI3Byb3BlcnR5LXJlYWRPbmx5XG5cdCAqIHRvIGxlYXJuIG1vcmUuXG5cdCAqL1xuXHRASW5wdXQoKSBzZXQgcmVhZE9ubHkoIGlzUmVhZE9ubHk6IGJvb2xlYW4gKSB7XG5cdFx0aWYgKCB0aGlzLmluc3RhbmNlICkge1xuXHRcdFx0dGhpcy5pbnN0YW5jZS5zZXRSZWFkT25seSggaXNSZWFkT25seSApO1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdC8vIERlbGF5IHNldHRpbmcgcmVhZC1vbmx5IHN0YXRlIHVudGlsIGVkaXRvciBpbml0aWFsaXphdGlvbi5cblx0XHR0aGlzLl9yZWFkT25seSA9IGlzUmVhZE9ubHk7XG5cdH1cblxuXHRnZXQgcmVhZE9ubHkoKTogYm9vbGVhbiB7XG5cdFx0aWYgKCB0aGlzLmluc3RhbmNlICkge1xuXHRcdFx0cmV0dXJuIHRoaXMuaW5zdGFuY2UucmVhZE9ubHk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRoaXMuX3JlYWRPbmx5O1xuXHR9XG5cblx0LyoqXG5cdCAqIEZpcmVzIHdoZW4gdGhlIGVkaXRvciBpcyByZWFkeS4gSXQgY29ycmVzcG9uZHMgd2l0aCB0aGUgYGVkaXRvciNpbnN0YW5jZVJlYWR5YFxuXHQgKiBodHRwczovL2NrZWRpdG9yLmNvbS9kb2NzL2NrZWRpdG9yNC9sYXRlc3QvYXBpL0NLRURJVE9SX2VkaXRvci5odG1sI2V2ZW50LWluc3RhbmNlUmVhZHlcblx0ICogZXZlbnQuXG5cdCAqL1xuXHRAT3V0cHV0KCkgcmVhZHkgPSBuZXcgRXZlbnRFbWl0dGVyPENLRWRpdG9yNC5FdmVudEluZm8+KCk7XG5cblx0LyoqXG5cdCAqIEZpcmVzIHdoZW4gdGhlIGNvbnRlbnQgb2YgdGhlIGVkaXRvciBoYXMgY2hhbmdlZC4gSXQgY29ycmVzcG9uZHMgd2l0aCB0aGUgYGVkaXRvciNjaGFuZ2VgXG5cdCAqIGh0dHBzOi8vY2tlZGl0b3IuY29tL2RvY3MvY2tlZGl0b3I0L2xhdGVzdC9hcGkvQ0tFRElUT1JfZWRpdG9yLmh0bWwjZXZlbnQtY2hhbmdlXG5cdCAqIGV2ZW50LiBGb3IgcGVyZm9ybWFuY2UgcmVhc29ucyB0aGlzIGV2ZW50IG1heSBiZSBjYWxsZWQgZXZlbiB3aGVuIGRhdGEgZGlkbid0IHJlYWxseSBjaGFuZ2VkLlxuXHQgKi9cblx0QE91dHB1dCgpIGNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8Q0tFZGl0b3I0LkV2ZW50SW5mbz4oKTtcblxuXHQvKipcblx0ICogRmlyZXMgd2hlbiB0aGUgY29udGVudCBvZiB0aGUgZWRpdG9yIGhhcyBjaGFuZ2VkLiBJbiBjb250cmFzdCB0byBgY2hhbmdlYCAtIG9ubHkgZW1pdHMgd2hlblxuXHQgKiBkYXRhIHJlYWxseSBjaGFuZ2VkIHRodXMgY2FuIGJlIHN1Y2Nlc3NmdWxseSB1c2VkIHdpdGggYFtkYXRhXWAgYW5kIHR3byB3YXkgYFsoZGF0YSldYCBiaW5kaW5nLlxuXHQgKlxuXHQgKiBTZWUgbW9yZTogaHR0cHM6Ly9hbmd1bGFyLmlvL2d1aWRlL3RlbXBsYXRlLXN5bnRheCN0d28td2F5LWJpbmRpbmctLS1cblx0ICovXG5cdEBPdXRwdXQoKSBkYXRhQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxDS0VkaXRvcjQuRXZlbnRJbmZvPigpO1xuXG5cdC8qKlxuXHQgKiBGaXJlcyB3aGVuIHRoZSBlZGl0aW5nIHZpZXcgb2YgdGhlIGVkaXRvciBpcyBmb2N1c2VkLiBJdCBjb3JyZXNwb25kcyB3aXRoIHRoZSBgZWRpdG9yI2ZvY3VzYFxuXHQgKiBodHRwczovL2NrZWRpdG9yLmNvbS9kb2NzL2NrZWRpdG9yNC9sYXRlc3QvYXBpL0NLRURJVE9SX2VkaXRvci5odG1sI2V2ZW50LWZvY3VzXG5cdCAqIGV2ZW50LlxuXHQgKi9cblx0QE91dHB1dCgpIGZvY3VzID0gbmV3IEV2ZW50RW1pdHRlcjxDS0VkaXRvcjQuRXZlbnRJbmZvPigpO1xuXG5cdC8qKlxuXHQgKiBGaXJlcyB3aGVuIHRoZSBlZGl0aW5nIHZpZXcgb2YgdGhlIGVkaXRvciBpcyBibHVycmVkLiBJdCBjb3JyZXNwb25kcyB3aXRoIHRoZSBgZWRpdG9yI2JsdXJgXG5cdCAqIGh0dHBzOi8vY2tlZGl0b3IuY29tL2RvY3MvY2tlZGl0b3I0L2xhdGVzdC9hcGkvQ0tFRElUT1JfZWRpdG9yLmh0bWwjZXZlbnQtYmx1clxuXHQgKiBldmVudC5cblx0ICovXG5cdEBPdXRwdXQoKSBibHVyID0gbmV3IEV2ZW50RW1pdHRlcjxDS0VkaXRvcjQuRXZlbnRJbmZvPigpO1xuXG5cdC8qKlxuXHQgKiBUaGUgaW5zdGFuY2Ugb2YgdGhlIGVkaXRvciBjcmVhdGVkIGJ5IHRoaXMgY29tcG9uZW50LlxuXHQgKi9cblx0aW5zdGFuY2U6IGFueTtcblxuXHQvKipcblx0ICogV3JhcHBlciBlbGVtZW50IHVzZWQgdG8gaW5pdGlhbGl6ZSBlZGl0b3IuXG5cdCAqL1xuXHR3cmFwcGVyOiBIVE1MRWxlbWVudDtcblxuXHQvKipcblx0ICogSWYgdGhlIGNvbXBvbmVudCBpcyByZWFk4oCTb25seSBiZWZvcmUgdGhlIGVkaXRvciBpbnN0YW5jZSBpcyBjcmVhdGVkLCBpdCByZW1lbWJlcnMgdGhhdCBzdGF0ZSxcblx0ICogc28gdGhlIGVkaXRvciBjYW4gYmVjb21lIHJlYWTigJNvbmx5IG9uY2UgaXQgaXMgcmVhZHkuXG5cdCAqL1xuXHRwcml2YXRlIF9yZWFkT25seTogYm9vbGVhbiA9IG51bGw7XG5cblx0LyoqXG5cdCAqIEEgY2FsbGJhY2sgZXhlY3V0ZWQgd2hlbiB0aGUgY29udGVudCBvZiB0aGUgZWRpdG9yIGNoYW5nZXMuIFBhcnQgb2YgdGhlXG5cdCAqIGBDb250cm9sVmFsdWVBY2Nlc3NvcmAgKGh0dHBzOi8vYW5ndWxhci5pby9hcGkvZm9ybXMvQ29udHJvbFZhbHVlQWNjZXNzb3IpIGludGVyZmFjZS5cblx0ICpcblx0ICogTm90ZTogVW5zZXQgdW5sZXNzIHRoZSBjb21wb25lbnQgdXNlcyB0aGUgYG5nTW9kZWxgLlxuXHQgKi9cblx0b25DaGFuZ2U/OiAoIGRhdGE6IHN0cmluZyApID0+IHZvaWQ7XG5cblx0LyoqXG5cdCAqIEEgY2FsbGJhY2sgZXhlY3V0ZWQgd2hlbiB0aGUgZWRpdG9yIGhhcyBiZWVuIGJsdXJyZWQuIFBhcnQgb2YgdGhlXG5cdCAqIGBDb250cm9sVmFsdWVBY2Nlc3NvcmAgKGh0dHBzOi8vYW5ndWxhci5pby9hcGkvZm9ybXMvQ29udHJvbFZhbHVlQWNjZXNzb3IpIGludGVyZmFjZS5cblx0ICpcblx0ICogTm90ZTogVW5zZXQgdW5sZXNzIHRoZSBjb21wb25lbnQgdXNlcyB0aGUgYG5nTW9kZWxgLlxuXHQgKi9cblx0b25Ub3VjaGVkPzogKCkgPT4gdm9pZDtcblxuXHRwcml2YXRlIF9kYXRhOiBzdHJpbmcgPSBudWxsO1xuXG5cdC8qKlxuXHQgKiBDS0VkaXRvciA0IHNjcmlwdCB1cmwgYWRkcmVzcy4gU2NyaXB0IHdpbGwgYmUgbG9hZGVkIG9ubHkgaWYgQ0tFRElUT1IgbmFtZXNwYWNlIGlzIG1pc3NpbmcuXG5cdCAqXG5cdCAqIERlZmF1bHRzIHRvICdodHRwczovL2Nkbi5ja2VkaXRvci5jb20vNC4xMi4xL3N0YW5kYXJkLWFsbC9ja2VkaXRvci5qcydcblx0ICovXG5cdEBJbnB1dCgpIGVkaXRvclVybCA9ICdodHRwczovL2Nkbi5ja2VkaXRvci5jb20vNC4xMi4xL3N0YW5kYXJkLWFsbC9ja2VkaXRvci5qcyc7XG5cblx0Y29uc3RydWN0b3IoIHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZiwgcHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSApIHtcblx0fVxuXG5cdG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcblx0XHRnZXRFZGl0b3JOYW1lc3BhY2UoIHRoaXMuZWRpdG9yVXJsICkudGhlbiggKCkgPT4ge1xuXHRcdFx0dGhpcy5uZ1pvbmUucnVuT3V0c2lkZUFuZ3VsYXIoIHRoaXMuY3JlYXRlRWRpdG9yLmJpbmQoIHRoaXMgKSApO1xuXHRcdH0gKS5jYXRjaCggd2luZG93LmNvbnNvbGUuZXJyb3IgKTtcblx0fVxuXG5cdG5nT25EZXN0cm95KCk6IHZvaWQge1xuXHRcdHRoaXMubmdab25lLnJ1bk91dHNpZGVBbmd1bGFyKCAoKSA9PiB7XG5cdFx0XHRpZiAoIHRoaXMuaW5zdGFuY2UgKSB7XG5cdFx0XHRcdHRoaXMuaW5zdGFuY2UuZGVzdHJveSgpO1xuXHRcdFx0XHR0aGlzLmluc3RhbmNlID0gbnVsbDtcblx0XHRcdH1cblx0XHR9ICk7XG5cdH1cblxuXHR3cml0ZVZhbHVlKCB2YWx1ZTogc3RyaW5nICk6IHZvaWQge1xuXHRcdHRoaXMuZGF0YSA9IHZhbHVlO1xuXHR9XG5cblx0cmVnaXN0ZXJPbkNoYW5nZSggY2FsbGJhY2s6ICggZGF0YTogc3RyaW5nICkgPT4gdm9pZCApOiB2b2lkIHtcblx0XHR0aGlzLm9uQ2hhbmdlID0gY2FsbGJhY2s7XG5cdH1cblxuXHRyZWdpc3Rlck9uVG91Y2hlZCggY2FsbGJhY2s6ICgpID0+IHZvaWQgKTogdm9pZCB7XG5cdFx0dGhpcy5vblRvdWNoZWQgPSBjYWxsYmFjaztcblx0fVxuXG5cdHByaXZhdGUgY3JlYXRlRWRpdG9yKCk6IHZvaWQge1xuXHRcdGNvbnN0IGVsZW1lbnQgPSB0aGlzLmNyZWF0ZUluaXRpYWxFbGVtZW50KCk7XG5cblx0XHR0aGlzLmNvbmZpZyA9IHRoaXMuZW5zdXJlRGl2YXJlYVBsdWdpbiggdGhpcy5jb25maWcgfHwge30gKTtcblxuXHRcdGNvbnN0IGluc3RhbmNlID0gdGhpcy50eXBlID09PSBDS0VkaXRvcjQuRWRpdG9yVHlwZS5JTkxJTkUgP1xuXHRcdFx0Q0tFRElUT1IuaW5saW5lKCBlbGVtZW50LCB0aGlzLmNvbmZpZyApXG5cdFx0XHQ6IENLRURJVE9SLnJlcGxhY2UoIGVsZW1lbnQsIHRoaXMuY29uZmlnICk7XG5cblx0XHRpbnN0YW5jZS5vbmNlKCAnaW5zdGFuY2VSZWFkeScsIGZ1bmN0aW9uKCBldnQgKSB7XG5cdFx0XHR0aGlzLmluc3RhbmNlID0gaW5zdGFuY2U7XG5cblx0XHRcdHRoaXMud3JhcHBlci5yZW1vdmVBdHRyaWJ1dGUoICdzdHlsZScgKTtcblxuXHRcdFx0dGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuYXBwZW5kQ2hpbGQoIHRoaXMud3JhcHBlciApO1xuXG5cdFx0XHQvLyBSZWFkIG9ubHkgc3RhdGUgbWF5IGNoYW5nZSBkdXJpbmcgaW5zdGFuY2UgaW5pdGlhbGl6YXRpb24uXG5cdFx0XHR0aGlzLnJlYWRPbmx5ID0gdGhpcy5fcmVhZE9ubHkgIT09IG51bGwgPyB0aGlzLl9yZWFkT25seSA6IHRoaXMuaW5zdGFuY2UucmVhZE9ubHk7XG5cblx0XHRcdHRoaXMuc3Vic2NyaWJlKCB0aGlzLmluc3RhbmNlICk7XG5cblx0XHRcdGNvbnN0IHVuZG8gPSBpbnN0YW5jZS51bmRvTWFuYWdlcjtcblxuXHRcdFx0aWYgKCB0aGlzLmRhdGEgIT09IG51bGwgKSB7XG5cdFx0XHRcdHVuZG8gJiYgdW5kby5sb2NrKCk7XG5cdFx0XHRcdGluc3RhbmNlLnNldERhdGEoIHRoaXMuZGF0YSApO1xuXG5cdFx0XHRcdC8vIExvY2tpbmcgdW5kb01hbmFnZXIgcHJldmVudHMgJ2NoYW5nZScgZXZlbnQuXG5cdFx0XHRcdC8vIFRyaWdnZXIgaXQgbWFudWFsbHkgdG8gdXBkYXRlZCBib3VuZCBkYXRhLlxuXHRcdFx0XHRpZiAoIHRoaXMuZGF0YSAhPT0gaW5zdGFuY2UuZ2V0RGF0YSgpICkge1xuXHRcdFx0XHRcdGluc3RhbmNlLmZpcmUoICdjaGFuZ2UnICk7XG5cdFx0XHRcdH1cblx0XHRcdFx0dW5kbyAmJiB1bmRvLnVubG9jaygpO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLm5nWm9uZS5ydW4oICgpID0+IHtcblx0XHRcdFx0dGhpcy5yZWFkeS5lbWl0KCBldnQgKTtcblx0XHRcdH0gKTtcblx0XHR9LCB0aGlzICk7XG5cdH1cblxuXHRwcml2YXRlIHN1YnNjcmliZSggZWRpdG9yOiBhbnkgKTogdm9pZCB7XG5cdFx0ZWRpdG9yLm9uKCAnZm9jdXMnLCBldnQgPT4ge1xuXHRcdFx0dGhpcy5uZ1pvbmUucnVuKCAoKSA9PiB7XG5cdFx0XHRcdHRoaXMuZm9jdXMuZW1pdCggZXZ0ICk7XG5cdFx0XHR9ICk7XG5cdFx0fSApO1xuXG5cdFx0ZWRpdG9yLm9uKCAnYmx1cicsIGV2dCA9PiB7XG5cdFx0XHR0aGlzLm5nWm9uZS5ydW4oICgpID0+IHtcblx0XHRcdFx0aWYgKCB0aGlzLm9uVG91Y2hlZCApIHtcblx0XHRcdFx0XHR0aGlzLm9uVG91Y2hlZCgpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0dGhpcy5ibHVyLmVtaXQoIGV2dCApO1xuXHRcdFx0fSApO1xuXHRcdH0gKTtcblxuXHRcdGVkaXRvci5vbiggJ2NoYW5nZScsIGV2dCA9PiB7XG5cdFx0XHR0aGlzLm5nWm9uZS5ydW4oICgpID0+IHtcblx0XHRcdFx0Y29uc3QgbmV3RGF0YSA9IGVkaXRvci5nZXREYXRhKCk7XG5cblx0XHRcdFx0dGhpcy5jaGFuZ2UuZW1pdCggZXZ0ICk7XG5cblx0XHRcdFx0aWYgKCBuZXdEYXRhID09PSB0aGlzLmRhdGEgKSB7XG5cdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0dGhpcy5fZGF0YSA9IG5ld0RhdGE7XG5cdFx0XHRcdHRoaXMuZGF0YUNoYW5nZS5lbWl0KCBuZXdEYXRhICk7XG5cblx0XHRcdFx0aWYgKCB0aGlzLm9uQ2hhbmdlICkge1xuXHRcdFx0XHRcdHRoaXMub25DaGFuZ2UoIG5ld0RhdGEgKTtcblx0XHRcdFx0fVxuXHRcdFx0fSApO1xuXHRcdH0gKTtcblx0fVxuXG5cdHByaXZhdGUgZW5zdXJlRGl2YXJlYVBsdWdpbiggY29uZmlnOiBDS0VkaXRvcjQuQ29uZmlnICk6IENLRWRpdG9yNC5Db25maWcge1xuXHRcdGxldCB7IGV4dHJhUGx1Z2lucywgcmVtb3ZlUGx1Z2lucyB9ID0gY29uZmlnO1xuXG5cdFx0ZXh0cmFQbHVnaW5zID0gdGhpcy5yZW1vdmVQbHVnaW4oIGV4dHJhUGx1Z2lucywgJ2RpdmFyZWEnICkgfHwgJyc7XG5cdFx0ZXh0cmFQbHVnaW5zID0gZXh0cmFQbHVnaW5zLmNvbmNhdCggdHlwZW9mIGV4dHJhUGx1Z2lucyA9PT0gJ3N0cmluZycgPyAnLGRpdmFyZWEnIDogJ2RpdmFyZWEnICk7XG5cblx0XHRpZiAoIHJlbW92ZVBsdWdpbnMgJiYgcmVtb3ZlUGx1Z2lucy5pbmNsdWRlcyggJ2RpdmFyZWEnICkgKSB7XG5cblx0XHRcdHJlbW92ZVBsdWdpbnMgPSB0aGlzLnJlbW92ZVBsdWdpbiggcmVtb3ZlUGx1Z2lucywgJ2RpdmFyZWEnICk7XG5cblx0XHRcdGNvbnNvbGUud2FybiggJ1tDS0VESVRPUl0gZGl2YXJlYSBwbHVnaW4gaXMgcmVxdWlyZWQgdG8gaW5pdGlhbGl6ZSBlZGl0b3IgdXNpbmcgQW5ndWxhciBpbnRlZ3JhdGlvbi4nICk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIE9iamVjdC5hc3NpZ24oIHt9LCBjb25maWcsIHsgZXh0cmFQbHVnaW5zLCByZW1vdmVQbHVnaW5zIH0gKTtcblx0fVxuXG5cdHByaXZhdGUgcmVtb3ZlUGx1Z2luKCBwbHVnaW5zOiBzdHJpbmcgfCBzdHJpbmdbXSwgdG9SZW1vdmU6IHN0cmluZyApOiBzdHJpbmcgfCBzdHJpbmdbXSB7XG5cdFx0aWYgKCAhcGx1Z2lucyApIHtcblx0XHRcdHJldHVybiBudWxsO1xuXHRcdH1cblxuXHRcdGNvbnN0IGlzU3RyaW5nID0gdHlwZW9mIHBsdWdpbnMgPT09ICdzdHJpbmcnO1xuXG5cdFx0aWYgKCBpc1N0cmluZyApIHtcblx0XHRcdHBsdWdpbnMgPSAoIHBsdWdpbnMgYXMgc3RyaW5nICkuc3BsaXQoICcsJyApO1xuXHRcdH1cblxuXHRcdHBsdWdpbnMgPSAoIHBsdWdpbnMgYXMgc3RyaW5nW10gKS5maWx0ZXIoIHBsdWdpbiA9PiBwbHVnaW4gIT09IHRvUmVtb3ZlICk7XG5cblx0XHRpZiAoIGlzU3RyaW5nICkge1xuXHRcdFx0cGx1Z2lucyA9ICggcGx1Z2lucyBhcyBzdHJpbmdbXSApLmpvaW4oICcsJyApO1xuXHRcdH1cblxuXHRcdHJldHVybiBwbHVnaW5zO1xuXHR9XG5cblx0cHJpdmF0ZSBjcmVhdGVJbml0aWFsRWxlbWVudCgpOiBIVE1MRWxlbWVudCB7XG5cdFx0Ly8gUmVuZGVyIGVkaXRvciBvdXRzaWRlIG9mIGNvbXBvbmVudCBzbyBpdCB3b24ndCBiZSByZW1vdmVkIGZyb20gRE9NIGJlZm9yZSBgaW5zdGFuY2VSZWFkeWAuXG5cdFx0dGhpcy53cmFwcGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCggJ2RpdicgKTtcblx0XHRjb25zdCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCggdGhpcy50YWdOYW1lICk7XG5cblx0XHR0aGlzLndyYXBwZXIuc2V0QXR0cmlidXRlKCAnc3R5bGUnLCAnZGlzcGxheTpub25lOycgKTtcblxuXHRcdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoIHRoaXMud3JhcHBlciApO1xuXHRcdHRoaXMud3JhcHBlci5hcHBlbmRDaGlsZCggZWxlbWVudCApO1xuXG5cdFx0cmV0dXJuIGVsZW1lbnQ7XG5cdH1cbn1cbiJdfQ==