/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
import { NgZone, EventEmitter, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { CKEditor4 } from './ckeditor';
export declare class CKEditorComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {
    private elementRef;
    private ngZone;
    /**
     * The configuration of the editor.
     * See https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html
     * to learn more.
     */
    config?: CKEditor4.Config;
    /**
     * Tag name of the editor component.
     *
     * The default tag is `textarea`.
     */
    tagName: string;
    /**
     * The type of the editor interface.
     *
     * By default editor interface will be initialized as `divarea` editor which is an inline editor with fixed UI.
     * You can change interface type by choosing between `divarea` and `inline` editor interface types.
     *
     * See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_uitypes.html
     * and https://ckeditor.com/docs/ckeditor4/latest/examples/fixedui.html
     * to learn more.
     */
    type: CKEditor4.EditorType;
    /**
     * Keeps track of the editor's data.
     *
     * It's also decorated as an input which is useful when not using the ngModel.
     *
     * See https://angular.io/api/forms/NgModel to learn more.
     */
    data: string;
    /**
     * When set `true`, the editor becomes read-only.
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#property-readOnly
     * to learn more.
     */
    readOnly: boolean;
    /**
     * Fires when the editor is ready. It corresponds with the `editor#instanceReady`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-instanceReady
     * event.
     */
    ready: EventEmitter<CKEditor4.EventInfo>;
    /**
     * Fires when the content of the editor has changed. It corresponds with the `editor#change`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-change
     * event. For performance reasons this event may be called even when data didn't really changed.
     */
    change: EventEmitter<CKEditor4.EventInfo>;
    /**
     * Fires when the content of the editor has changed. In contrast to `change` - only emits when
     * data really changed thus can be successfully used with `[data]` and two way `[(data)]` binding.
     *
     * See more: https://angular.io/guide/template-syntax#two-way-binding---
     */
    dataChange: EventEmitter<CKEditor4.EventInfo>;
    /**
     * Fires when the editing view of the editor is focused. It corresponds with the `editor#focus`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-focus
     * event.
     */
    focus: EventEmitter<CKEditor4.EventInfo>;
    /**
     * Fires when the editing view of the editor is blurred. It corresponds with the `editor#blur`
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-blur
     * event.
     */
    blur: EventEmitter<CKEditor4.EventInfo>;
    /**
     * The instance of the editor created by this component.
     */
    instance: any;
    /**
     * Wrapper element used to initialize editor.
     */
    wrapper: HTMLElement;
    /**
     * If the component is read–only before the editor instance is created, it remembers that state,
     * so the editor can become read–only once it is ready.
     */
    private _readOnly;
    /**
     * A callback executed when the content of the editor changes. Part of the
     * `ControlValueAccessor` (https://angular.io/api/forms/ControlValueAccessor) interface.
     *
     * Note: Unset unless the component uses the `ngModel`.
     */
    onChange?: (data: string) => void;
    /**
     * A callback executed when the editor has been blurred. Part of the
     * `ControlValueAccessor` (https://angular.io/api/forms/ControlValueAccessor) interface.
     *
     * Note: Unset unless the component uses the `ngModel`.
     */
    onTouched?: () => void;
    private _data;
    /**
     * CKEditor 4 script url address. Script will be loaded only if CKEDITOR namespace is missing.
     *
     * Defaults to 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js'
     */
    editorUrl: string;
    constructor(elementRef: ElementRef, ngZone: NgZone);
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    writeValue(value: string): void;
    registerOnChange(callback: (data: string) => void): void;
    registerOnTouched(callback: () => void): void;
    private createEditor;
    private subscribe;
    private ensureDivareaPlugin;
    private removePlugin;
    private createInitialElement;
}
