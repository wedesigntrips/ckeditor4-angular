import { CommonModule } from '@angular/common';
import { Component, NgZone, Input, Output, EventEmitter, forwardRef, ElementRef, NgModule } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import loadScript from 'load-script';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var promise;
/**
 * @param {?} editorURL
 * @return {?}
 */
function getEditorNamespace(editorURL) {
    if (editorURL.length < 1) {
        throw new TypeError('CKEditor URL must be a non-empty string.');
    }
    if ('CKEDITOR' in window) {
        return Promise.resolve(CKEDITOR);
    }
    else if (!promise) {
        promise = new Promise((/**
         * @param {?} scriptResolve
         * @param {?} scriptReject
         * @return {?}
         */
        function (scriptResolve, scriptReject) {
            loadScript(editorURL, (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                if (err) {
                    scriptReject(err);
                }
                else {
                    scriptResolve(CKEDITOR);
                    promise = undefined;
                }
            }));
        }));
    }
    return promise;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CKEditorComponent = /** @class */ (function () {
    function CKEditorComponent(elementRef, ngZone) {
        this.elementRef = elementRef;
        this.ngZone = ngZone;
        /**
         * Tag name of the editor component.
         *
         * The default tag is `textarea`.
         */
        this.tagName = 'textarea';
        /**
         * The type of the editor interface.
         *
         * By default editor interface will be initialized as `divarea` editor which is an inline editor with fixed UI.
         * You can change interface type by choosing between `divarea` and `inline` editor interface types.
         *
         * See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_uitypes.html
         * and https://ckeditor.com/docs/ckeditor4/latest/examples/fixedui.html
         * to learn more.
         */
        this.type = "divarea" /* DIVAREA */;
        /**
         * Fires when the editor is ready. It corresponds with the `editor#instanceReady`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-instanceReady
         * event.
         */
        this.ready = new EventEmitter();
        /**
         * Fires when the content of the editor has changed. It corresponds with the `editor#change`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-change
         * event. For performance reasons this event may be called even when data didn't really changed.
         */
        this.change = new EventEmitter();
        /**
         * Fires when the content of the editor has changed. In contrast to `change` - only emits when
         * data really changed thus can be successfully used with `[data]` and two way `[(data)]` binding.
         *
         * See more: https://angular.io/guide/template-syntax#two-way-binding---
         */
        this.dataChange = new EventEmitter();
        /**
         * Fires when the editing view of the editor is focused. It corresponds with the `editor#focus`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-focus
         * event.
         */
        this.focus = new EventEmitter();
        /**
         * Fires when the editing view of the editor is blurred. It corresponds with the `editor#blur`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-blur
         * event.
         */
        this.blur = new EventEmitter();
        /**
         * If the component is read–only before the editor instance is created, it remembers that state,
         * so the editor can become read–only once it is ready.
         */
        this._readOnly = null;
        this._data = null;
        /**
         * CKEditor 4 script url address. Script will be loaded only if CKEDITOR namespace is missing.
         *
         * Defaults to 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js'
         */
        this.editorUrl = 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js';
    }
    Object.defineProperty(CKEditorComponent.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return this._data;
        },
        /**
         * Keeps track of the editor's data.
         *
         * It's also decorated as an input which is useful when not using the ngModel.
         *
         * See https://angular.io/api/forms/NgModel to learn more.
         */
        set: /**
         * Keeps track of the editor's data.
         *
         * It's also decorated as an input which is useful when not using the ngModel.
         *
         * See https://angular.io/api/forms/NgModel to learn more.
         * @param {?} data
         * @return {?}
         */
        function (data) {
            if (data === this._data) {
                return;
            }
            if (this.instance) {
                this.instance.setData(data);
                // Data may be changed by ACF.
                this._data = this.instance.getData();
                return;
            }
            this._data = data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CKEditorComponent.prototype, "readOnly", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.instance) {
                return this.instance.readOnly;
            }
            return this._readOnly;
        },
        /**
         * When set `true`, the editor becomes read-only.
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#property-readOnly
         * to learn more.
         */
        set: /**
         * When set `true`, the editor becomes read-only.
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#property-readOnly
         * to learn more.
         * @param {?} isReadOnly
         * @return {?}
         */
        function (isReadOnly) {
            if (this.instance) {
                this.instance.setReadOnly(isReadOnly);
                return;
            }
            // Delay setting read-only state until editor initialization.
            this._readOnly = isReadOnly;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        getEditorNamespace(this.editorUrl).then((/**
         * @return {?}
         */
        function () {
            _this.ngZone.runOutsideAngular(_this.createEditor.bind(_this));
        })).catch(window.console.error);
    };
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        function () {
            if (_this.instance) {
                _this.instance.destroy();
                _this.instance = null;
            }
        }));
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CKEditorComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.data = value;
    };
    /**
     * @param {?} callback
     * @return {?}
     */
    CKEditorComponent.prototype.registerOnChange = /**
     * @param {?} callback
     * @return {?}
     */
    function (callback) {
        this.onChange = callback;
    };
    /**
     * @param {?} callback
     * @return {?}
     */
    CKEditorComponent.prototype.registerOnTouched = /**
     * @param {?} callback
     * @return {?}
     */
    function (callback) {
        this.onTouched = callback;
    };
    /**
     * @private
     * @return {?}
     */
    CKEditorComponent.prototype.createEditor = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var element = this.createInitialElement();
        this.config = this.ensureDivareaPlugin(this.config || {});
        /** @type {?} */
        var instance = this.type === "inline" /* INLINE */ ?
            CKEDITOR.inline(element, this.config)
            : CKEDITOR.replace(element, this.config);
        instance.once('instanceReady', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            var _this = this;
            this.instance = instance;
            this.wrapper.removeAttribute('style');
            this.elementRef.nativeElement.appendChild(this.wrapper);
            // Read only state may change during instance initialization.
            this.readOnly = this._readOnly !== null ? this._readOnly : this.instance.readOnly;
            this.subscribe(this.instance);
            /** @type {?} */
            var undo = instance.undoManager;
            if (this.data !== null) {
                undo && undo.lock();
                instance.setData(this.data);
                // Locking undoManager prevents 'change' event.
                // Trigger it manually to updated bound data.
                if (this.data !== instance.getData()) {
                    instance.fire('change');
                }
                undo && undo.unlock();
            }
            this.ngZone.run((/**
             * @return {?}
             */
            function () {
                _this.ready.emit(evt);
            }));
        }), this);
    };
    /**
     * @private
     * @param {?} editor
     * @return {?}
     */
    CKEditorComponent.prototype.subscribe = /**
     * @private
     * @param {?} editor
     * @return {?}
     */
    function (editor) {
        var _this = this;
        editor.on('focus', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                _this.focus.emit(evt);
            }));
        }));
        editor.on('blur', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                if (_this.onTouched) {
                    _this.onTouched();
                }
                _this.blur.emit(evt);
            }));
        }));
        editor.on('change', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var newData = editor.getData();
                _this.change.emit(evt);
                if (newData === _this.data) {
                    return;
                }
                _this._data = newData;
                _this.dataChange.emit(newData);
                if (_this.onChange) {
                    _this.onChange(newData);
                }
            }));
        }));
    };
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    CKEditorComponent.prototype.ensureDivareaPlugin = /**
     * @private
     * @param {?} config
     * @return {?}
     */
    function (config) {
        var extraPlugins = config.extraPlugins, removePlugins = config.removePlugins;
        extraPlugins = this.removePlugin(extraPlugins, 'divarea') || '';
        extraPlugins = extraPlugins.concat(typeof extraPlugins === 'string' ? ',divarea' : 'divarea');
        if (removePlugins && removePlugins.includes('divarea')) {
            removePlugins = this.removePlugin(removePlugins, 'divarea');
            console.warn('[CKEDITOR] divarea plugin is required to initialize editor using Angular integration.');
        }
        return Object.assign({}, config, { extraPlugins: extraPlugins, removePlugins: removePlugins });
    };
    /**
     * @private
     * @param {?} plugins
     * @param {?} toRemove
     * @return {?}
     */
    CKEditorComponent.prototype.removePlugin = /**
     * @private
     * @param {?} plugins
     * @param {?} toRemove
     * @return {?}
     */
    function (plugins, toRemove) {
        if (!plugins) {
            return null;
        }
        /** @type {?} */
        var isString = typeof plugins === 'string';
        if (isString) {
            plugins = ((/** @type {?} */ (plugins))).split(',');
        }
        plugins = ((/** @type {?} */ (plugins))).filter((/**
         * @param {?} plugin
         * @return {?}
         */
        function (plugin) { return plugin !== toRemove; }));
        if (isString) {
            plugins = ((/** @type {?} */ (plugins))).join(',');
        }
        return plugins;
    };
    /**
     * @private
     * @return {?}
     */
    CKEditorComponent.prototype.createInitialElement = /**
     * @private
     * @return {?}
     */
    function () {
        // Render editor outside of component so it won't be removed from DOM before `instanceReady`.
        this.wrapper = document.createElement('div');
        /** @type {?} */
        var element = document.createElement(this.tagName);
        this.wrapper.setAttribute('style', 'display:none;');
        document.body.appendChild(this.wrapper);
        this.wrapper.appendChild(element);
        return element;
    };
    CKEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ckeditor',
                    template: '<ng-template></ng-template>',
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return CKEditorComponent; })),
                            multi: true,
                        }
                    ]
                }] }
    ];
    /** @nocollapse */
    CKEditorComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NgZone }
    ]; };
    CKEditorComponent.propDecorators = {
        config: [{ type: Input }],
        tagName: [{ type: Input }],
        type: [{ type: Input }],
        data: [{ type: Input }],
        readOnly: [{ type: Input }],
        ready: [{ type: Output }],
        change: [{ type: Output }],
        dataChange: [{ type: Output }],
        focus: [{ type: Output }],
        blur: [{ type: Output }],
        editorUrl: [{ type: Input }]
    };
    return CKEditorComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
/**
 * Basic typings for the CKEditor4 elements.
 */
var CKEditor4;
(function (CKEditor4) {
    /**
     * The CKEditor4 editor constructor.
     * @record
     */
    function Config() { }
    CKEditor4.Config = Config;
    /**
     * The event object passed to CKEditor4 event callbacks.
     *
     * See https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_eventInfo.html
     * to learn more.
     * @record
     */
    function EventInfo() { }
    CKEditor4.EventInfo = EventInfo;
})(CKEditor4 || (CKEditor4 = {}));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CKEditorModule = /** @class */ (function () {
    function CKEditorModule() {
    }
    CKEditorModule.decorators = [
        { type: NgModule, args: [{
                    imports: [FormsModule, CommonModule],
                    declarations: [CKEditorComponent],
                    exports: [CKEditorComponent]
                },] }
    ];
    return CKEditorModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { CKEditorModule, CKEditorComponent, CKEditor4 };

//# sourceMappingURL=ckeditor4-angular.js.map