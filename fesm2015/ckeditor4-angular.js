import { CommonModule } from '@angular/common';
import { Component, NgZone, Input, Output, EventEmitter, forwardRef, ElementRef, NgModule } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import loadScript from 'load-script';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let promise;
/**
 * @param {?} editorURL
 * @return {?}
 */
function getEditorNamespace(editorURL) {
    if (editorURL.length < 1) {
        throw new TypeError('CKEditor URL must be a non-empty string.');
    }
    if ('CKEDITOR' in window) {
        return Promise.resolve(CKEDITOR);
    }
    else if (!promise) {
        promise = new Promise((/**
         * @param {?} scriptResolve
         * @param {?} scriptReject
         * @return {?}
         */
        (scriptResolve, scriptReject) => {
            loadScript(editorURL, (/**
             * @param {?} err
             * @return {?}
             */
            err => {
                if (err) {
                    scriptReject(err);
                }
                else {
                    scriptResolve(CKEDITOR);
                    promise = undefined;
                }
            }));
        }));
    }
    return promise;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CKEditorComponent {
    /**
     * @param {?} elementRef
     * @param {?} ngZone
     */
    constructor(elementRef, ngZone) {
        this.elementRef = elementRef;
        this.ngZone = ngZone;
        /**
         * Tag name of the editor component.
         *
         * The default tag is `textarea`.
         */
        this.tagName = 'textarea';
        /**
         * The type of the editor interface.
         *
         * By default editor interface will be initialized as `divarea` editor which is an inline editor with fixed UI.
         * You can change interface type by choosing between `divarea` and `inline` editor interface types.
         *
         * See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_uitypes.html
         * and https://ckeditor.com/docs/ckeditor4/latest/examples/fixedui.html
         * to learn more.
         */
        this.type = "divarea" /* DIVAREA */;
        /**
         * Fires when the editor is ready. It corresponds with the `editor#instanceReady`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-instanceReady
         * event.
         */
        this.ready = new EventEmitter();
        /**
         * Fires when the content of the editor has changed. It corresponds with the `editor#change`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-change
         * event. For performance reasons this event may be called even when data didn't really changed.
         */
        this.change = new EventEmitter();
        /**
         * Fires when the content of the editor has changed. In contrast to `change` - only emits when
         * data really changed thus can be successfully used with `[data]` and two way `[(data)]` binding.
         *
         * See more: https://angular.io/guide/template-syntax#two-way-binding---
         */
        this.dataChange = new EventEmitter();
        /**
         * Fires when the editing view of the editor is focused. It corresponds with the `editor#focus`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-focus
         * event.
         */
        this.focus = new EventEmitter();
        /**
         * Fires when the editing view of the editor is blurred. It corresponds with the `editor#blur`
         * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#event-blur
         * event.
         */
        this.blur = new EventEmitter();
        /**
         * If the component is read–only before the editor instance is created, it remembers that state,
         * so the editor can become read–only once it is ready.
         */
        this._readOnly = null;
        this._data = null;
        /**
         * CKEditor 4 script url address. Script will be loaded only if CKEDITOR namespace is missing.
         *
         * Defaults to 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js'
         */
        this.editorUrl = 'https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js';
    }
    /**
     * Keeps track of the editor's data.
     *
     * It's also decorated as an input which is useful when not using the ngModel.
     *
     * See https://angular.io/api/forms/NgModel to learn more.
     * @param {?} data
     * @return {?}
     */
    set data(data) {
        if (data === this._data) {
            return;
        }
        if (this.instance) {
            this.instance.setData(data);
            // Data may be changed by ACF.
            this._data = this.instance.getData();
            return;
        }
        this._data = data;
    }
    /**
     * @return {?}
     */
    get data() {
        return this._data;
    }
    /**
     * When set `true`, the editor becomes read-only.
     * https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#property-readOnly
     * to learn more.
     * @param {?} isReadOnly
     * @return {?}
     */
    set readOnly(isReadOnly) {
        if (this.instance) {
            this.instance.setReadOnly(isReadOnly);
            return;
        }
        // Delay setting read-only state until editor initialization.
        this._readOnly = isReadOnly;
    }
    /**
     * @return {?}
     */
    get readOnly() {
        if (this.instance) {
            return this.instance.readOnly;
        }
        return this._readOnly;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        getEditorNamespace(this.editorUrl).then((/**
         * @return {?}
         */
        () => {
            this.ngZone.runOutsideAngular(this.createEditor.bind(this));
        })).catch(window.console.error);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        () => {
            if (this.instance) {
                this.instance.destroy();
                this.instance = null;
            }
        }));
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.data = value;
    }
    /**
     * @param {?} callback
     * @return {?}
     */
    registerOnChange(callback) {
        this.onChange = callback;
    }
    /**
     * @param {?} callback
     * @return {?}
     */
    registerOnTouched(callback) {
        this.onTouched = callback;
    }
    /**
     * @private
     * @return {?}
     */
    createEditor() {
        /** @type {?} */
        const element = this.createInitialElement();
        this.config = this.ensureDivareaPlugin(this.config || {});
        /** @type {?} */
        const instance = this.type === "inline" /* INLINE */ ?
            CKEDITOR.inline(element, this.config)
            : CKEDITOR.replace(element, this.config);
        instance.once('instanceReady', (/**
         * @param {?} evt
         * @return {?}
         */
        function (evt) {
            this.instance = instance;
            this.wrapper.removeAttribute('style');
            this.elementRef.nativeElement.appendChild(this.wrapper);
            // Read only state may change during instance initialization.
            this.readOnly = this._readOnly !== null ? this._readOnly : this.instance.readOnly;
            this.subscribe(this.instance);
            /** @type {?} */
            const undo = instance.undoManager;
            if (this.data !== null) {
                undo && undo.lock();
                instance.setData(this.data);
                // Locking undoManager prevents 'change' event.
                // Trigger it manually to updated bound data.
                if (this.data !== instance.getData()) {
                    instance.fire('change');
                }
                undo && undo.unlock();
            }
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                this.ready.emit(evt);
            }));
        }), this);
    }
    /**
     * @private
     * @param {?} editor
     * @return {?}
     */
    subscribe(editor) {
        editor.on('focus', (/**
         * @param {?} evt
         * @return {?}
         */
        evt => {
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                this.focus.emit(evt);
            }));
        }));
        editor.on('blur', (/**
         * @param {?} evt
         * @return {?}
         */
        evt => {
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                if (this.onTouched) {
                    this.onTouched();
                }
                this.blur.emit(evt);
            }));
        }));
        editor.on('change', (/**
         * @param {?} evt
         * @return {?}
         */
        evt => {
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const newData = editor.getData();
                this.change.emit(evt);
                if (newData === this.data) {
                    return;
                }
                this._data = newData;
                this.dataChange.emit(newData);
                if (this.onChange) {
                    this.onChange(newData);
                }
            }));
        }));
    }
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    ensureDivareaPlugin(config) {
        let { extraPlugins, removePlugins } = config;
        extraPlugins = this.removePlugin(extraPlugins, 'divarea') || '';
        extraPlugins = extraPlugins.concat(typeof extraPlugins === 'string' ? ',divarea' : 'divarea');
        if (removePlugins && removePlugins.includes('divarea')) {
            removePlugins = this.removePlugin(removePlugins, 'divarea');
            console.warn('[CKEDITOR] divarea plugin is required to initialize editor using Angular integration.');
        }
        return Object.assign({}, config, { extraPlugins, removePlugins });
    }
    /**
     * @private
     * @param {?} plugins
     * @param {?} toRemove
     * @return {?}
     */
    removePlugin(plugins, toRemove) {
        if (!plugins) {
            return null;
        }
        /** @type {?} */
        const isString = typeof plugins === 'string';
        if (isString) {
            plugins = ((/** @type {?} */ (plugins))).split(',');
        }
        plugins = ((/** @type {?} */ (plugins))).filter((/**
         * @param {?} plugin
         * @return {?}
         */
        plugin => plugin !== toRemove));
        if (isString) {
            plugins = ((/** @type {?} */ (plugins))).join(',');
        }
        return plugins;
    }
    /**
     * @private
     * @return {?}
     */
    createInitialElement() {
        // Render editor outside of component so it won't be removed from DOM before `instanceReady`.
        this.wrapper = document.createElement('div');
        /** @type {?} */
        const element = document.createElement(this.tagName);
        this.wrapper.setAttribute('style', 'display:none;');
        document.body.appendChild(this.wrapper);
        this.wrapper.appendChild(element);
        return element;
    }
}
CKEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ckeditor',
                template: '<ng-template></ng-template>',
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => CKEditorComponent)),
                        multi: true,
                    }
                ]
            }] }
];
/** @nocollapse */
CKEditorComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: NgZone }
];
CKEditorComponent.propDecorators = {
    config: [{ type: Input }],
    tagName: [{ type: Input }],
    type: [{ type: Input }],
    data: [{ type: Input }],
    readOnly: [{ type: Input }],
    ready: [{ type: Output }],
    change: [{ type: Output }],
    dataChange: [{ type: Output }],
    focus: [{ type: Output }],
    blur: [{ type: Output }],
    editorUrl: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
/**
 * Basic typings for the CKEditor4 elements.
 */
var CKEditor4;
(function (CKEditor4) {
    /**
     * The CKEditor4 editor constructor.
     * @record
     */
    function Config() { }
    CKEditor4.Config = Config;
    /**
     * The event object passed to CKEditor4 event callbacks.
     *
     * See https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_eventInfo.html
     * to learn more.
     * @record
     */
    function EventInfo() { }
    CKEditor4.EventInfo = EventInfo;
})(CKEditor4 || (CKEditor4 = {}));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CKEditorModule {
}
CKEditorModule.decorators = [
    { type: NgModule, args: [{
                imports: [FormsModule, CommonModule],
                declarations: [CKEditorComponent],
                exports: [CKEditorComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { CKEditorModule, CKEditorComponent, CKEditor4 };

//# sourceMappingURL=ckeditor4-angular.js.map