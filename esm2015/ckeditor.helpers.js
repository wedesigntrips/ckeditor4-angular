/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
import loadScript from 'load-script';
/** @type {?} */
let promise;
/**
 * @param {?} editorURL
 * @return {?}
 */
export function getEditorNamespace(editorURL) {
    if (editorURL.length < 1) {
        throw new TypeError('CKEditor URL must be a non-empty string.');
    }
    if ('CKEDITOR' in window) {
        return Promise.resolve(CKEDITOR);
    }
    else if (!promise) {
        promise = new Promise((/**
         * @param {?} scriptResolve
         * @param {?} scriptReject
         * @return {?}
         */
        (scriptResolve, scriptReject) => {
            loadScript(editorURL, (/**
             * @param {?} err
             * @return {?}
             */
            err => {
                if (err) {
                    scriptReject(err);
                }
                else {
                    scriptResolve(CKEDITOR);
                    promise = undefined;
                }
            }));
        }));
    }
    return promise;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2tlZGl0b3IuaGVscGVycy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NrZWRpdG9yNC1hbmd1bGFyLyIsInNvdXJjZXMiOlsiY2tlZGl0b3IuaGVscGVycy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUtBLE9BQU8sVUFBVSxNQUFNLGFBQWEsQ0FBQzs7SUFHakMsT0FBTzs7Ozs7QUFFWCxNQUFNLFVBQVUsa0JBQWtCLENBQUUsU0FBaUI7SUFDcEQsSUFBSyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRztRQUMzQixNQUFNLElBQUksU0FBUyxDQUFFLDBDQUEwQyxDQUFFLENBQUM7S0FDbEU7SUFFRCxJQUFLLFVBQVUsSUFBSSxNQUFNLEVBQUc7UUFDM0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFFLFFBQVEsQ0FBRSxDQUFDO0tBQ25DO1NBQU0sSUFBSyxDQUFDLE9BQU8sRUFBRztRQUN0QixPQUFPLEdBQUcsSUFBSSxPQUFPOzs7OztRQUFFLENBQUUsYUFBYSxFQUFFLFlBQVksRUFBRyxFQUFFO1lBQ3hELFVBQVUsQ0FBRSxTQUFTOzs7O1lBQUUsR0FBRyxDQUFDLEVBQUU7Z0JBQzVCLElBQUssR0FBRyxFQUFHO29CQUNWLFlBQVksQ0FBRSxHQUFHLENBQUUsQ0FBQztpQkFDcEI7cUJBQU07b0JBQ04sYUFBYSxDQUFFLFFBQVEsQ0FBRSxDQUFDO29CQUMxQixPQUFPLEdBQUcsU0FBUyxDQUFDO2lCQUNwQjtZQUNGLENBQUMsRUFBRSxDQUFDO1FBQ0wsQ0FBQyxFQUFFLENBQUM7S0FDSjtJQUVELE9BQU8sT0FBTyxDQUFDO0FBQ2hCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEBsaWNlbnNlIENvcHlyaWdodCAoYykgMjAwMy0yMDE5LCBDS1NvdXJjZSAtIEZyZWRlcmljbyBLbmFiYmVuLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICogRm9yIGxpY2Vuc2luZywgc2VlIExJQ0VOU0UubWQuXG4gKi9cblxuaW1wb3J0IGxvYWRTY3JpcHQgZnJvbSAnbG9hZC1zY3JpcHQnO1xuXG5kZWNsYXJlIGxldCBDS0VESVRPUjogYW55O1xubGV0IHByb21pc2U7XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRFZGl0b3JOYW1lc3BhY2UoIGVkaXRvclVSTDogc3RyaW5nICk6IFByb21pc2U8eyBbIGtleTogc3RyaW5nIF06IGFueTsgfT4ge1xuXHRpZiAoIGVkaXRvclVSTC5sZW5ndGggPCAxICkge1xuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoICdDS0VkaXRvciBVUkwgbXVzdCBiZSBhIG5vbi1lbXB0eSBzdHJpbmcuJyApO1xuXHR9XG5cblx0aWYgKCAnQ0tFRElUT1InIGluIHdpbmRvdyApIHtcblx0XHRyZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCBDS0VESVRPUiApO1xuXHR9IGVsc2UgaWYgKCAhcHJvbWlzZSApIHtcblx0XHRwcm9taXNlID0gbmV3IFByb21pc2UoICggc2NyaXB0UmVzb2x2ZSwgc2NyaXB0UmVqZWN0ICkgPT4ge1xuXHRcdFx0bG9hZFNjcmlwdCggZWRpdG9yVVJMLCBlcnIgPT4ge1xuXHRcdFx0XHRpZiAoIGVyciApIHtcblx0XHRcdFx0XHRzY3JpcHRSZWplY3QoIGVyciApO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHNjcmlwdFJlc29sdmUoIENLRURJVE9SICk7XG5cdFx0XHRcdFx0cHJvbWlzZSA9IHVuZGVmaW5lZDtcblx0XHRcdFx0fVxuXHRcdFx0fSApO1xuXHRcdH0gKTtcblx0fVxuXG5cdHJldHVybiBwcm9taXNlO1xufVxuIl19