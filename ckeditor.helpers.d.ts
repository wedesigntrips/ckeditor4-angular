/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */
export declare function getEditorNamespace(editorURL: string): Promise<{
    [key: string]: any;
}>;
